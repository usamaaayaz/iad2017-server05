
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

dbObject = {
  url:'mongodb://localhost:27017/iadserver05'
}

dbObject.performDbOperation = function(callbackMethod){
    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");
        callbackMethod(db, function() {
            db.close();
        });
   
    });

}
  

module.exports = dbObject

