var express = require('express');
var dbObject = require('../models/mongo');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');


  
  
});

router.get('/users', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('/add', function(req, res, next) {
  

    var body = req.body;

  ///*** Add Operation to a document */

   if(body!=null && body.data.length>0){

        dbObject.performDbOperation(function(db,callback){
            var collection = db.collection('users');
            collection.insertMany(body.data, function(err, result) {
              console.log("Inserted 3 users into the user collection");
              callback(result);
              res.json(result);
            });
            

        });

   }
///***  */


});

router.post('/update',function(req,res,next){

   dbObject.performDbOperation(function(db,callback){
      var collection = db.collection('users');
      collection.updateOne({
       _id : ObjectID("5900879473446842852d77a3")
      },{
        $set:{
          email:"barak02@diego.com",
          name:"Barak diego 02"
        }
      },function(err,result){
          res.json(result);  
          callback(result);
      })
   });


});


router.get('/all',function(req,res,next){
     

   params = {};
   params = req.params;  
   dbObject.performDbOperation(function(db,callback){
      var collection = db.collection("users");
      collection.find(params).toArray(function(err,result){
          res.json(result);
      });

   });


});


router.get('/find',function(req,res,next){
     

   params = {};
   userid = req.query.userid;  
   dbObject.performDbOperation(function(db,callback){
      var collection = db.collection("users");
      collection.find({"_id":ObjectID(userid)}).toArray(function(err,result){
          res.json(result);
      });

   });


});


router.post('/uploadFile',upload.array('image', 12),function(req,res,next){
    
  
});








module.exports = router;
